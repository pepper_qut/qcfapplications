<?xml version="1.0" encoding="UTF-8" ?>
<Package name="GetStarted" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="style" src="html/css/style.css" />
        <File name="BlinkingPepper" src="html/images/BlinkingPepper.gif" />
        <File name="back" src="html/images/back.png" />
        <File name="forward" src="html/images/forward.png" />
        <File name="left" src="html/images/left.png" />
        <File name="leftTurn" src="html/images/leftTurn.png" />
        <File name="logo2" src="html/images/logo2.png" />
        <File name="quit" src="html/images/quit.png" />
        <File name="right" src="html/images/right.png" />
        <File name="rightTurn" src="html/images/rightTurn.png" />
        <File name="walking" src="html/images/walking.png" />
        <File name="index" src="html/index.html" />
        <File name="Connection" src="html/js/Connection.js" />
        <File name="HelloBubble" src="html/images/HelloBubble.png" />
        <File name="StartIcon" src="html/images/StartIcon.png" />
        <File name="icon" src="icon.png" />
        <File name="Envelope" src="html/images/Envelope.png" />
        <File name="dance" src="html/images/dance.png" />
        <File name="DontDance" src="html/images/DontDance.png" />
        <File name="FastDance" src="html/images/FastDance.png" />
        <File name="SlowDance" src="html/images/SlowDance.png" />
        <File name="TaiChi" src="html/images/TaiChi.png" />
        <File name="WhatBubble" src="html/images/WhatBubble.png" />
        <File name="CheckOutMyMoves" src="html/images/CheckOutMyMoves.png" />
        <File name="CakeSong" src="html/images/CakeSong.png" />
        <File name="DontSing" src="html/images/DontSing.png" />
        <File name="SingAlong" src="html/images/SingAlong.png" />
        <File name="SurpriseSong" src="html/images/SurpriseSong.png" />
        <File name="SingAlongText" src="html/images/SingAlongText.png" />
        <File name="LetsPlay" src="html/images/LetsPlay.png" />
        <File name="PepperSays" src="html/images/PepperSays.png" />
        <File name="StopPlay" src="html/images/StopPlay.png" />
        <File name="StarSong" src="html/images/StarSong.png" />
        <File name="CHF-LogoStacked" src="html/images/CHF-LogoStacked.jpg" />
        <File name="eventLogo" src="html/images/eventLogo.png" />
        <File name="GreetBubble" src="html/images/GreetBubble.png" />
        <File name="eventLogoWhite" src="html/images/eventLogoWhite.png" />
        <File name="Cross" src="html/images/Cross.png" />
        <File name="Tick" src="html/images/Tick.png" />
        <File name="SafeDance" src="html/images/SafeDance.png" />
        <File name="background" src="html/background.png" />
        <File name="HelloBubble300" src="html/images/HelloBubble300.png" />
        <File name="WhatBubble300" src="html/images/WhatBubble300.png" />
        <File name="CHF-LogoStacked400" src="html/images/CHF-LogoStacked400.jpg" />
        <File name="CHF-LogoStacked420" src="html/images/CHF-LogoStacked420.jpg" />
        <File name="CHFlogo" src="html/images/CHFlogo.png" />
        <File name="TellAJoke" src="html/images/TellAJoke.png" />
        <File name="Settings" src="html/images/Settings.png" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
