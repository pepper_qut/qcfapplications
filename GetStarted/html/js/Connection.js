/****************************************************/
/* Aldebaran Behavior Complementary Development Kit */
/* ConnectionHtmlChoregraphe: Connection.js         */
/* Innovation - Protolab - mcaniot@aldebaran.com    */
/* Aldebaran Robotics (c) 2016 All Rights Reserved. */
/* This file is confidential.                       */
/* NOTE: THIS FILE IS PUBLISHED ONLINE               */
/****************************************************/

/* Create a session for the connection */
var session = new QiSession();

function startSubscribe() {
	session.service("ALTextToSpeech").done(function (ALTextToSpeech) {
		ALTextToSpeech.setParameter("speed",85);
	});
	session.service("ALMemory").done(function (ALMemory) {
        ALMemory.subscriber("FrontTactilTouched").done(function(subscriber) {
            subscriber.signal.connect(shush);
        });   
		ALMemory.subscriber("MiddleTactilTouched").done(function(subscriber) {
            subscriber.signal.connect(shush);
        });
		ALMemory.subscriber("RearTactilTouched").done(function(subscriber) {
            subscriber.signal.connect(shush);
        });
    });
}

function cueBehavior(behaviorName) {
	session.service("ALTabletService").done(function (ALTabletService) {
		ALTabletService.hideWebview();	
		session.service("ALMemory").done(function (ALMemory) {
			ALMemory.raiseEvent("cueBehavior", behaviorName);
		});
	});
}

function sayText(response) {
	session.service("ALAnimatedSpeech").done(function (tts) {
		tts.say(response);
	});
}

function shush() {
	session.service("ALTextToSpeech").done(function (ALTextToSpeech) {
		ALTextToSpeech.stopAll();
	});
}