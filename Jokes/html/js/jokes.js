var jokes = [
	["What's brown, hairy and wears sunglasses?","A coconut on vacation!"],
	["What does a spider's bride wear?","A webbing dress."], 
	["What did one toilet say to the other?","You look a bit flushed!"],
	["Where do cows go for entertainment?","The mooooo-vies!"],
	["What did one firefly say to the other?","You glow, girl"],
	["Why did the cookie go to the doctor?","It was feeling crummy."], 
	["How do you know when the moon has had enough to eat?","When it's full!"], 
	["Where does spaghetti go to dance?","The meat-ball."], 
	["How does the ocean say hello?","It waves."], 
	["Why was the math book sad?","Because it had so many problems."], 
	["What time would it be if Godzilla came to school?","Time to run!"], 
	["Why did the dog do so well in school?","Because he was the teacher's pet!"], 
	["Why did the computer go to the doctor?","It had a virus."], 
	["What are the strongest days of the week?","Saturday and Sunday. The rest are weak days."], 
	["What animal can you always find at a baseball game?","A bat!"], 
	["What can you catch, but never throw?","A cold!"],
	["What gets wet while it's drying?","A towel!"],
	["What do you get when you cross a vampire and a snowman?","Frost bite!"],
	["What did the Dalmatian say after lunch?","That hit the spot!"],
	["Why did the kid cross the playground?","To get to the other slide."],
	["What did the little corn say to the mama corn?","Where is pop corn?"],
	["Who did the zombie take to the prom?","His ghoul friend!"], 
	["What did the lettuce say to the celery?","Quit stalking me!"],
	["What key do you use to open a banana?","A monkey."],
	["What is a plumber's favourite vegetable?","A leek."],
];
var jokesAloud = [
	["What's brown, hairy, and wears sunglasses?","A coconut on vacation!"],
	["What does a spider's bride wear?","A webbing dress."], 
	["What did one toilet say to the other?","You look a bit flushed!"],
	["Where do cows go for entertainment?","The moo vees!"],
	["What did one firefly say to the other?","You glow girl"],
	["Why did the cookie go to the doctor?","It was feeling crummy."], 
	["How do you know when the moon has had enough to eat?","When it's full!"], 
	["Where does spaghetti go to dance?","The meatball."], 
	["How does the ocean say hello?","It waves."], 
	["Why was the math book sad?","Because it had so many problems."], 
	["What time would it be if Godzilla came to school?","Time to run!"], 
	["Why did the dog do so well in school?","Because he was the teacher's pet!"], 
	["Why did the computer go to the doctor?","It had a virus."], 
	["What are the strongest days of the week?","Saturday and Sunday. The rest are weekdays."], 
	["What animal can you always find at a baseball game?","A bat!"], 
	["What can you catch, but never throw?","A cold!"],
	["What gets wet while it's drying?","A towel!"],
	["What do you get when you cross a vampire and a snowman?","Frost bite!"],
	["What did the dalmatian say after lunch?","That hit the spot!"],
	["Why did the kid cross the playground?","To get to the other slide."],
	["What did the little corn say to the mama corn?","Where is pop corn?"],
	["Who did the zombie take to the prom?","His ghoul friend!"], 
	["What did the lettuce say to the celery?","Quit stalking me!"],
	["What key do you use to open a banana?","A monkey."],
	["What is a plumber's favourite vegetable?","A leek."],
];

var question = "Question";
var answer = "Answer";

var questionAloud = "Question";
var answerAloud = "Answer";

var rIndex = -1;
var randArray;

function nextJoke() {
	jokeCount = jokes.length;
	//note(rIndex);
	if (rIndex <0 || rIndex == jokeCount-1) {
		randArray = new Array(jokeCount);
		for(var n = 0; n<jokeCount ; n++){
			randArray[n] = n;
		}
		note(randArray);
		for(var i = jokeCount-1; i > 0; i--){
		  var j = Math.floor(Math.random() * i)
		  var temp = randArray[i]
		  randArray[i] = randArray[j]
		  randArray[j] = temp
		}
		note(randArray);
		rIndex = 0;
	} else {
		rIndex++;
	}
	return randArray[rIndex];
}

function tellJoke() {
	var jIndex = nextJoke();
	question = jokes[jIndex][0];
	answer = jokes[jIndex][1];
	questionAloud = jokesAloud[jIndex][0];
	answerAloud = jokesAloud[jIndex][1];
	document.getElementById("jokeQ").innerHTML = question;
	document.getElementById("jokeA").innerHTML = "";
	document.getElementById("tellJoke").style.display = "none";
	document.getElementById("punchline").style.display = "flex";
	sayText(questionAloud);
}

function tellAgain() {
	sayText(questionAloud);
}

function tellPunchline() {
	document.getElementById("jokeA").innerHTML = answer;
	document.getElementById("tellJoke").style.display = "flex";
	document.getElementById("punchline").style.display = "none";
	sayText(answerAloud);
}

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min) ) + min;
}