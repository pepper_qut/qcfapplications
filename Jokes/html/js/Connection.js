/* Create a session for the connection */
var session = new QiSession();
var myDialog = null;
var pg = "intro";
var waitingPage = null;
var correctAns = false;

function note(str) {
	record = document.getElementById("debug").innerHTML;
	document.getElementById("debug").innerHTML = str +"<br>"+record;
}

function react(response) {
	check = pg + response;
	document.getElementById(check).checked="checked";
}

function endSpeech() {
	if (waitingPage && waitingPage.style.display=='none') {
		waitingPage.style.display = 'flex';
	} 
}

function startSubscribe() {
	session.service("ALMemory").done(function (ALMemory) {
		memory = ALMemory;
        ALMemory.subscriber("answer").done(function(subscriber) {
            subscriber.signal.connect(react);
        }); 
        ALMemory.subscriber("ALAnimatedSpeech/EndOfAnimatedSpeech").done(function(subscriber) {
            subscriber.signal.connect(endSpeech);
        });   
    });
	session.service('ALBehaviorManager').then(function (service) {
		behaviourManager = service;
	});
	session.service("ALTextToSpeech").done(function (service) {
		service.setParameter("speed",85);
    });
	session.service("ALAnimatedSpeech").done(function (service) {
		tts = service;
    });
	session.service("ALTabletService").done(function (service) {
		tabletService = service;
    });
	session.service("ALBasicAwareness").done(function (service) {
		basicAwareness = service;
		basicAwareness.setEnabled(true);
		// Stop Pepper looking down at tablet every time you touch it!
		basicAwareness.setStimulusDetectionEnabled("TabletTouch", false);
    });
	session.service("ALBackgroundMovement").done(function (service) {
		backgroundMovement = service;
		// Control background movement
		// backgroundMovement.setEnabled(false);
    });
}

function sayText(response, config) {
	tts.say(response, config);
	memory.raiseEvent("writeData", response);
}

function show(elementID) {
	// set background movement back on
	backgroundMovement.setEnabled(true);
	
                // try to find the requested page and alert if it's not found
                var ele = document.getElementById(elementID);
                if (!ele) {
                    alert("no such page");
                    return;
                }
				waitingPage = ele; // In case we have to wait for speech to finish

                // get all pages, loop through them and hide them
                var pages = document.getElementsByClassName('page');
                for(var i = 0; i < pages.length; i++) {
                    pages[i].style.display = 'none';
                }

                // then show the requested page
                //ele.style.display = 'flex';
}

function stopApp() {
	behaviourName = path.replace("/apps/", "")+"/behavior_1";
	behaviourManager.stopBehavior(behaviourName);
}

function setQuestion(topic,quest) {
	Q = quest+1;
	if (Q <= topic.length) {
		showProgress(Q,topic.length)
		show("QuizPage");
		correctAns = false;
		sayText(topic[quest][0][1], {"bodyLanguageMode":"contextual"});
		responseCount = topic[quest].length;
		document.getElementById('question').innerHTML = topic[quest][0][0];
		document.getElementById('textA').innerHTML = topic[quest][1][0]; document.getElementById('a').checked = false;
		document.getElementById('textB').innerHTML = topic[quest][2][0]; document.getElementById('b').checked = false;
		document.getElementById('c').checked = false;
		document.getElementById('d').checked = false;
		if (responseCount > 3) {
			document.getElementById('textC').innerHTML = topic[quest][3][0]; document.getElementById('opt3').style.visibility = 'visible';
			document.getElementById('textD').innerHTML = topic[quest][4][0]; document.getElementById('opt4').style.visibility = 'visible';
		} else {
			document.getElementById('opt3').style.visibility = 'hidden';
			document.getElementById('opt4').style.visibility = 'hidden';
		};
	} else {
		hideProgress;
		returnToStart("Well done. Would you like to try a quiz on another topic?");
	};
}
function showProgress(pageNum,pageCount) {
	var progressBar = document.getElementById("progress");
    if (progressBar) {
        //alert("Progress = "+parseInt(i)+" out of "+parsInt(pageCount));
		progressBar.style.visibility = "hidden";
		var progress = parseInt(pageNum*100/pageCount);
		styleStr = progress+"vw";
		//alert(styleStr);
		progressBar.style.width = styleStr;
		progressBar.style.visibility = "visible";
    }
}
function hideProgress() {
	document.getElementById("progress").style.visibility = "hidden";
}

function setQuiz(quiz, introText) {
	Quiz = quiz;
	showProgress(1,10)
	show("getSetPage");
	document.getElementById('question').innerHTML = "<span style='color: yellow;'>&#8593; Progress Bar</span>";
	sayText(introText+"\\pau=200\\Tap next when you're ready to get started");
	/*setQuestion(Quiz,0);*/
}

function respond(topic,quest,resp) {
	correctAns = topic[quest-1][resp][2];
	ansStr = anim(correctAns);
	ansStr += topic[quest-1][resp][1];
	sayText(ansStr, {"bodyLanguageMode":"disabled"});
}

function anim(correctAns) {
	if (correctAns) {
		animSet = postiveReactions;
	} else {
		animSet = negativeReactions;
	}
	ceil = animSet.length;
	var i = Math.floor(Math.random() * ceil);
	return animSet[i];
}
function returnToStart(msg) {
	hideProgress();
	show("IntroPage");
	sayText(msg,{"bodyLanguageMode":"contextual"});
	document.getElementById('question').innerHTML = "Hello, my name is Pepper.</br>Would you like to try a short quiz?";
}