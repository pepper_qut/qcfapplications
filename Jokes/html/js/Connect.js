/* Create a session for the connection */
var session = new QiSession();

function note(str) {
	record = document.getElementById("debug").innerHTML;
	document.getElementById("debug").innerHTML = str +"<br>"+record;
}

function endSpeech() {
	note("speech ended");
}

function startSubscribe() {
	//console.log("starting"); 
	if (session) {	
		session.service('ALBehaviorManager').then(function (service) {
			behaviourManager = service;
		});
		session.service("ALTextToSpeech").done(function (service) {
			service.setParameter("speed",85);
		});
		session.service("ALAnimatedSpeech").done(function (service) {
			tts = service;
			tts.say("\\rspd=85\\Great, I know some funny jokes. Tap the button on my screen and I'll tell you one of them.");
		});
	} else {
		//console.log("no session");
		window.speechObj = new SpeechSynthesisUtterance();
		window.speechObj.voice = speechSynthesis.getVoices().filter(function(voice) {console.log(voice); return voice.lang == "en-GB"; })[0];
		//console.log(window.speechObj);
	}
	/* Not required for this application
	session.service("ALTabletService").done(function (service) {
		tabletService = service;
    });
	session.service("ALBasicAwareness").done(function (service) {
		basicAwareness = service;
		basicAwareness.setEnabled(true);
		// Stop Pepper looking down at tablet every time you touch it!
		basicAwareness.setStimulusDetectionEnabled("TabletTouch", false);
    });
	session.service("ALBackgroundMovement").done(function (service) {
		backgroundMovement = service;
		// Control background movement
		// backgroundMovement.setEnabled(false);
    });
	*/
}

function sayText(sentence) {
	if (session) {
		tts.say(sentence);
	} else {
		console.log(window.speechObj);
		if ('speechSynthesis' in window) {
			/* Voice won't be set the first time, so set it here (This is weird in Chrome) */
			if (!window.speechObj.voice) {
				window.speechObj.voice = speechSynthesis.getVoices().filter(function(voice) {console.log(voice); return voice.lang == "en-GB"; })[0];
			} else {
				console.log(window.speechObj.voice);
			}
			window.speechObj.text = sentence;
			window.speechSynthesis.speak(speechObj);
		} else {
			document.getElementById("jokeA").innerHTML = "Sorry, I'm speechless";
		}
	}
}

function stopApp() {
	pathName = window.location.pathname;
	path = pathName.slice(0,pathName.lastIndexOf('/'));
	behaviourName = path.replace("/apps/", "")+"/behavior_1";
	behaviourManager.stopBehavior(behaviourName);
}