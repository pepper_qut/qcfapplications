<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Jokes" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="Connect" src="html/js/Connect.js" />
        <File name="Connection" src="html/js/Connection.js" />
        <File name="style" src="html/css/style.css" />
        <File name="BlinkingPepper" src="html/images/BlinkingPepper.gif" />
        <File name="CHF-LogoStacked" src="html/images/CHF-LogoStacked.jpg" />
        <File name="Cross" src="html/images/Cross.png" />
        <File name="StopPlay" src="html/images/StopPlay.png" />
        <File name="eventLogo" src="html/images/eventLogo.png" />
        <File name="eventLogoWhite" src="html/images/eventLogoWhite.png" />
        <File name="left" src="html/images/left.png" />
        <File name="logo2" src="html/images/logo2.png" />
        <File name="quit" src="html/images/quit.png" />
        <File name="right" src="html/images/right.png" />
        <File name="index" src="html/index.html" />
        <File name="jokes" src="html/js/jokes.js" />
        <File name="laughFace" src="html/images/laughFace.png" />
        <File name="Qmark" src="html/images/Qmark.png" />
        <File name="question" src="html/images/question.png" />
        <File name="repeat" src="html/images/repeat.png" />
        <File name="index2" src="html/index2.html" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
