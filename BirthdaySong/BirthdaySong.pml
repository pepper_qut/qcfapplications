<?xml version="1.0" encoding="UTF-8" ?>
<Package name="BirthdaySong" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="Cake" src="html/images/Cake.png" />
        <File name="HappyBirthday" src="html/images/HappyBirthday.mp3" />
        <File name="HappyBirthdayModern" src="html/images/HappyBirthdayModern.mp3" />
        <File name="black" src="html/images/black.png" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
