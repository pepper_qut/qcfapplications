<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Animated Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Happy birthday, to you.</source>
            <comment>Text</comment>
            <translation type="obsolete">Happy birthday, to you.</translation>
        </message>
        <message>
            <source>Happy. Birthday. To. You.</source>
            <comment>Text</comment>
            <translation type="obsolete">Happy. Birthday. To. You.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Happy birthday to you.</source>
            <comment>Text</comment>
            <translation type="obsolete">Happy birthday to you.</translation>
        </message>
    </context>
</TS>
