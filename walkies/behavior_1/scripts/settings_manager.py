from naoqi import ALProxy
from naoqi import ALBroker
from naoqi import ALModule
from optparse import OptionParser
import time
import sys
import re

SettingsManager = False

class SettingsManagerModule(ALModule):
	def __init__(self, name):
		ALModule.__init__(self, name)

	def save(self, options):
		'''
		Sets the body tracking default setting
		'''
		f = open('/home/nao/.config/naoqi/autonomy.cfg')
		contents = f.read()
		f.close()

		for key, value in options:
			module, method = key.split('.')

			match =  re.search(r'Start BasicAwareness Tracking[\s\S]+?Module:[^A-Z]+' +
							   module + r'\n[^A-Za-z]+Method:[^A-Za-z]+' +
							   method + r'\n[^A-Za-z]+Parameters:\s+([^\n]+)',
							   contents)

			contents = contents[:match.start(1)] + str(value) + contents[match.end(1):]

		f = open('/home/nao/.config/naoqi/autonomy.cfg', 'w')
		f.write(contents)
		f.close()


def main():
	parser = OptionParser()
	parser.add_option('--pip',
	    help='Parent broker port. The IP address or your robot',
	    dest='pip')
	parser.add_option('--pport',
	    help='Parent broker port. The port NAOqi is listening to',
	    dest='pport',
	    type='int')
	parser.set_defaults(
	    pip='127.0.0.1',
	    pport=9559)

	(opts, args_) = parser.parse_args()
	pip   = opts.pip
	pport = opts.pport

	broker = ALBroker('settings_manager_broker', '0.0.0.0', 0, pip, pport)


	# Warning: SettingsManager must be a global variable
	# The name given to the constructor must be the name of the
	# variable
	global SettingsManager
	SettingsManager = SettingsManagerModule('SettingsManager')

	try:
		while True:
			time.sleep(5)
	except KeyboardInterrupt:
		print '\nInterrupted by user, shutting down'
		broker.shutdown()
		sys.exit(0)



if __name__ == '__main__':
    main()
