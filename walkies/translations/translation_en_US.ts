<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Let's go</name>
        <message>
            <source>OK, let's go!</source>
            <comment>Text</comment>
            <translation type="obsolete">OK, let's go!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Lets Go</name>
        <message>
            <source>OK, let's go!</source>
            <comment>Text</comment>
            <translation type="obsolete">OK, let's go!</translation>
        </message>
        <message>
            <source>Let's go!</source>
            <comment>Text</comment>
            <translation type="obsolete">Let's go!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Oh</name>
        <message>
            <source>Oh.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh.</translation>
        </message>
        <message>
            <source>OK</source>
            <comment>Text</comment>
            <translation type="obsolete">OK</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Oh dear</name>
        <message>
            <source>Oh dear. Please wait a moment.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh dear. Please wait a moment.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Oh dear (1)</name>
        <message>
            <source>Oh dear. Please wait a moment.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh dear. Please wait a moment.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>oh</source>
            <comment>Text</comment>
            <translation type="obsolete">oh</translation>
        </message>
        <message>
            <source>oh. please wait a moment.</source>
            <comment>Text</comment>
            <translation type="obsolete">oh. please wait a moment.</translation>
        </message>
        <message>
            <source>Oh dear. Please wait a moment.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh dear. Please wait a moment.</translation>
        </message>
        <message>
            <source>Here we are</source>
            <comment>Text</comment>
            <translation type="obsolete">Here we are</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (1)</name>
        <message>
            <source>OK, Let's go</source>
            <comment>Text</comment>
            <translation type="obsolete">OK, Let's go</translation>
        </message>
        <message>
            <source>OK let's go.</source>
            <comment>Text</comment>
            <translation type="obsolete">OK let's go.</translation>
        </message>
        <message>
            <source>OK, let's go.</source>
            <comment>Text</comment>
            <translation type="obsolete">OK, let's go.</translation>
        </message>
        <message>
            <source>OK. Ready.</source>
            <comment>Text</comment>
            <translation type="obsolete">OK. Ready.</translation>
        </message>
        <message>
            <source>OK. Let's go.</source>
            <comment>Text</comment>
            <translation type="obsolete">OK. Let's go.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (2)</name>
        <message>
            <source>OK</source>
            <comment>Text</comment>
            <translation type="obsolete">OK</translation>
        </message>
    </context>
</TS>
