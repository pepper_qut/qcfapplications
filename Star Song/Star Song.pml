<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Star Song" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="TwinkleStar" src="html/images/TwinkleStar.gif" />
        <File name="icon" src="html/images/icon.png" />
        <File name="star" src="html/images/star.png" />
        <File name="icon" src="icon.png" />
        <File name="twinkleSingle" src="html/images/twinkleSingle.wav" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
