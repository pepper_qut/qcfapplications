import qi
import re

SettingsManager = False

class SettingsManager:

	def save(self, options):
		'''
		Sets the body tracking default setting
		'''
		f = open('/home/nao/.config/naoqi/autonomy.cfg')
		contents = f.read()
		f.close()

		for key in options:
			module, method = key.split('.')
			value = options[key]

			print key + ':', value

			match =  re.search(r'Start BasicAwareness Tracking[\s\S]+?Module:[^A-Z]+' +
							   module + r'\n[^A-Za-z]+Method:[^A-Za-z]+' +
							   method + r'\n[^A-Za-z]+Parameters:\s+([^\n]+)',
							   contents)

			if match:
				contents = contents[:match.start(1)] + str(value) + contents[match.end(1):]

			match =  re.search(r'Wake up[\s\S]+?Module:[^A-Z]+' +
							   module + r'\n[^A-Za-z]+Method:[^A-Za-z]+' +
							   method + r'\n[^A-Za-z]+Parameters:\s+([^\n]+)',
							   contents)

			if match:
				contents = contents[:match.start(1)] + str(value) + contents[match.end(1):]

		f = open('/home/nao/.config/naoqi/autonomy.cfg', 'w')
		f.write(contents)
		f.close()

def main():
    app = qi.Application()
    app.start()
    session = app.session
    settings_manager = SettingsManager()
    session.registerService("SettingsManager", settings_manager)
    app.run()

if __name__ == "__main__":
    main()
