<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Settings" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="README" src="README.md" />
        <File name="styles" src="html/css/styles.css" />
        <File name="toggle_left" src="html/images/toggle_left.png" />
        <File name="toggle_off" src="html/images/toggle_off.png" />
        <File name="toggle_on" src="html/images/toggle_on.png" />
        <File name="toggle_right" src="html/images/toggle_right.png" />
        <File name="index" src="html/index.html" />
        <File name="jquery-3.2.1.min" src="html/js/jquery-3.2.1.min.js" />
        <File name="settings" src="html/js/settings.js" />
        <File name="autonomy" src="config/autonomy.cfg" />
        <File name="settings_manager" src="scripts/settings_manager.py" />
        <File name="speaker_icon" src="html/images/speaker_icon.svg" />
        <File name="qiproject" src="qiproject.xml" />
        <File name="SettingsIcon" src="SettingsIcon.png" />
        <File name="icon" src="icon.png" />
        <File name="brightness" src="html/images/brightness.png" />
        <File name="readme" src="html/readme.html" />
        <File name="colorschememapping" src="html/readme_files/colorschememapping.xml" />
        <File name="filelist" src="html/readme_files/filelist.xml" />
        <File name="image001" src="html/readme_files/image001.png" />
        <File name="image002" src="html/readme_files/image002.png" />
        <File name="image003" src="html/readme_files/image003.jpg" />
        <File name="image004" src="html/readme_files/image004.jpg" />
        <File name="themedata" src="html/readme_files/themedata.thmx" />
        <File name="PIN" src="html/PIN.txt" />
        <File name="pin" src="html/pin.html" />
        <File name="style" src="html/css/style.css" />
        <File name="Connection" src="html/js/Connection.js" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
    <qipython name="settings_manager" />
</Package>
