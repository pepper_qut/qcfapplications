# README #

This is a custom settings package for use with the Pepper robot. 

It provides the user with the ability to enable/disable basic awareness and body rotation, and to save these settings to persist between boots. 

Additionally, volume and screen brightness controls are provided. (tapping on the image of the speaker will cause Pepper to say the phrase "Is this better?" which is useful for finding a good volume)
